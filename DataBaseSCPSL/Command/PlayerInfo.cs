﻿using System;
using System.Globalization;
using DataBaseSCPSL;
using Smod2;
using Smod2.API;
using Smod2.Commands;

namespace Command
{
    internal class PlayerInfo : ICommandHandler
    {
        private Main main;

        public PlayerInfo(Main main)
        {
            this.main = main;
        }

        public string GetCommandDescription()
        {
            return "Get information from player";
        }

        public string GetUsage()
        {
            return "INFO <PLAYER | STEAMID> <Name of the Player | SteamID64 of the player>";
        }

        public string[] OnCall(ICommandSender sender, string[] args)
        {
            DataBaseSCPSL.Manager.DBConnection db = new DataBaseSCPSL.Manager.DBConnection(main);
            string table = ConfigManager.Manager.Config.GetStringValue("dbsl_sql_table", "rp");

            if(args[0] == "player" || args[0] == "p")
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (p.Name.Contains(args[1]))
                    {
                        string steamid = p.SteamId.Trim("::ffff:".ToCharArray());
                        DateTime firstjoin = db.GetDateTime("first_join", steamid, table.ToString());
                        return new string[] {
                            $"--- Info du joueur {p.Name} --- \n" +
                            $"SteamID : {steamid}" +
                            $"Rank : {db.GetString("rank", steamid, table)}\n" +
                            $"Kills : {db.GetInt("kills", steamid, table)}\n" +
                            $"Morts : {db.GetInt("deaths", steamid, table)}\n" +
                            $"TeamKill : {db.GetInt("teamkills", steamid, table)}\n" +
                            $"C'est échappé {db.GetInt("escape", steamid, table)} fois.\n\n" +
                            $"Nombre de parties jouées {db.GetInt("gameplayed", steamid, table)} fois.\n\n" +

                            $"Nombre de spawn en \n\n" +

                            $"SCP 049 : {db.GetInt("class_scp049", steamid, table)}\n" +
                            $"SCP 049-2 / Zombie : {db.GetInt("class_scp0492", steamid, table)}\n" +
                            $"SCP 079 : {db.GetInt("class_scp079", steamid, table)}\n" +
                            $"SCP 096 : {db.GetInt("class_scp096", steamid, table)}\n" +
                            $"SCP 106 : {db.GetInt("class_scp106", steamid, table)}\n" +
                            $"SCP 173 : {db.GetInt("class_scp173", steamid, table)}\n" +
                            $"SCP 939 : {db.GetInt("class_scp939", steamid, table)}\n\n" +

                            $"Classe D : {db.GetInt("class_classd", steamid, table)}\n" +
                            $"Insurection du Chaos : {db.GetInt("class_ci", steamid, table)}\n\n" +

                            $"Scientifique : {db.GetInt("class_scientist", steamid, table)}\n" +
                            $"NTF Commandant : {db.GetInt("class_ntfcmd", steamid, table)}\n" +
                            $"NTF Scientifique : {db.GetInt("class_ntf_scientist", steamid, table)}\n" +
                            $"NTF Lieutenant : {db.GetInt("class_ntfltn", steamid, table)}\n" +
                            $"NTF Cadet : {db.GetInt("class_ntf_cd", steamid, table)}\n" +
                            $"Guarde : {db.GetInt("class_fguard", steamid, table)}\n\n" +

                            $"Tutoriel : {db.GetInt("class_tut", steamid, table)}\n"
                        };

                    }
                    else
                    {
                        continue;
                    }
                }
                return new string[] { "Joueur non trouvé" };

            }else if(args[0] == "steamid" || args[0] == "s")
            {
                if(args[1].Length == 17)
                {
                    if (db.GetString("rank", args[1], table) != null)
                    {
                        string steamid = args[1];
                        DateTime firstjoin = db.GetDateTime("first_join", steamid, table.ToString());
                        return new string[] {
                            $"--- Info du joueur {db.GetString("username", steamid, table)} --- \n" +
                            $"Rank : {db.GetString("rank", steamid, table)}\n" +
                            $"Kills : {db.GetInt("kills", steamid, table)}\n" +
                            $"Morts : {db.GetInt("deaths", steamid, table)}\n" +
                            $"TeamKill : {db.GetInt("teamkills", steamid, table)}\n" +
                            $"C'est échappé {db.GetInt("escape", steamid, table)} fois.\n\n" +
                            $"Nombre de spawn en \n\n" +

                            $"SCP 049 : {db.GetInt("class_scp049", steamid, table)}\n" +
                            $"SCP 049-2 / Zombie : {db.GetInt("class_scp0492", steamid, table)}\n" +
                            $"SCP 079 : {db.GetInt("class_scp079", steamid, table)}\n" +
                            $"SCP 096 : {db.GetInt("class_scp096", steamid, table)}\n" +
                            $"SCP 106 : {db.GetInt("class_scp106", steamid, table)}\n" +
                            $"SCP 173 : {db.GetInt("class_scp173", steamid, table)}\n" +
                            $"SCP 939 : {db.GetInt("class_scp939", steamid, table)}\n\n" +

                            $"Classe D : {db.GetInt("class_classd", steamid, table)}\n" +
                            $"Insurection du Chaos : {db.GetInt("class_ci", steamid, table)}\n\n" +

                            $"Scientifique : {db.GetInt("class_scientist", steamid, table)}\n" +
                            $"NTF Commandant : {db.GetInt("class_ntfcmd", steamid, table)}\n" +
                            $"NTF Scientifique : {db.GetInt("class_ntf_scientist", steamid, table)}\n" +
                            $"NTF Lieutenant : {db.GetInt("class_ntfltn", steamid, table)}\n" +
                            $"NTF Cadet : {db.GetInt("class_ntf_cd", steamid, table)}\n" +
                            $"Guarde : {db.GetInt("class_fguard", steamid, table)}\n\n" +

                            $"Tutoriel : {db.GetInt("class_tut", steamid, table)}\n"
                        };
                    }
                    else
                    {
                        return new string[] { "Joueur non trouvé." };
                    }
                }
                else
                {
                    return new string[] { "SteamID invalide." };
                }
            }
            else
            {
                return new string[] { GetUsage() };
            }   
        }
    }
}