﻿using System;
using System.Linq;
using DataBaseSCPSL;
using DataBaseSCPSL.Manager;
using Smod2;
using Smod2.API;
using Smod2.EventHandlers;
using Smod2.Events;

namespace Handler
{
    internal class EventHandlers : IEventHandlerPlayerJoin, IEventHandlerWaitingForPlayers, IEventHandlerSetRole, IEventHandlerPlayerDie, IEventHandlerCheckEscape, IEventHandlerRoundEnd
    {
        private Main main;

        public EventHandlers(Main main)
        {
            this.main = main;
        }

        private string table = ConfigManager.Manager.Config.GetStringValue("dbsql_sql_table", "stats").TrimEnd(' ').TrimStart(' ');

        public void OnCheckEscape(PlayerCheckEscapeEvent ev)
        {
            if(ev.AllowEscape == true)
            {
                DBConnection db = new DBConnection(main);
                string steamid = ev.Player.SteamId.Trim("::ffff:".ToCharArray());
                db.IncrementINT("escape", steamid, table, 1);
            }
        }

        public void OnPlayerDie(PlayerDeathEvent ev)
        {
            DBConnection db = new DBConnection(main);
            int[] teamNTF = { (int)Team.MTF, (int)Team.RSC }, teamChaos = { (int)Team.CHI, (int)Team.CDP };

            string vsteamid = ev.Player.SteamId.Trim("::ffff:".ToCharArray());
            string ksteamid = ev.Player.SteamId.Trim("::ffff:".ToCharArray());

            Player v = ev.Player;
            Player k = ev.Killer;

            if (v.Name == "Server" || v.Name == string.Empty  || v.Name == "Dedicated Server") { return; }

            db.IncrementINT("deaths", vsteamid, table, 1);

            if (k.Name == "Server" || k.Name == string.Empty || k.Name == "Dedicated Server" || k.Name == "World") { return; }

            if (ev.DamageTypeVar == Smod2.API.DamageType.CONTAIN || ev.DamageTypeVar == Smod2.API.DamageType.DECONT || ev.DamageTypeVar == Smod2.API.DamageType.FALLDOWN || ev.DamageTypeVar == Smod2.API.DamageType.FLYING || ev.DamageTypeVar == Smod2.API.DamageType.LURE || ev.DamageTypeVar == Smod2.API.DamageType.NONE || ev.DamageTypeVar == Smod2.API.DamageType.NUKE || ev.DamageTypeVar == Smod2.API.DamageType.POCKET || ev.DamageTypeVar == Smod2.API.DamageType.RAGDOLLLESS || ev.DamageTypeVar == Smod2.API.DamageType.TESLA || ev.DamageTypeVar == Smod2.API.DamageType.WALL) { return; }

            bool checkteamkill()
            {
                if (teamNTF.Contains((int)v.TeamRole.Team) && teamNTF.Contains((int)k.TeamRole.Team))
                    return true;
                if (teamChaos.Contains((int)v.TeamRole.Team) && teamChaos.Contains((int)k.TeamRole.Team))
                    return true;
                return false;
            }

            if(checkteamkill() == true){ db.IncrementINT("teamkills", ksteamid, table, 1); } else { db.IncrementINT("kills", ksteamid, table, 1); }


        }

        public void OnPlayerJoin(PlayerJoinEvent ev)
        {
            DBConnection db = new DBConnection(main);
            string steamid = ev.Player.SteamId.Trim("::ffff:".ToCharArray());
            db.AddPlayer(steamid, ev.Player.Name);
            db.UpdateValue(steamid, "last_connexion", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), table);
            db.UpdateValue(steamid, "username", ev.Player.Name, table);

            if (ev.Player.GetUserGroup().Name.Length > 0)
            {
                db.UpdateValue(steamid, "rank", ev.Player.GetUserGroup().Name, table);
            }
            else
            {
                db.UpdateValue(steamid, "rank", "user", table);
            }
        }

        public void OnSetRole(PlayerSetRoleEvent ev)
        {
            DBConnection db = new DBConnection(main);
            string steamid = ev.Player.SteamId.Trim("::ffff:".ToCharArray());

            switch (ev.TeamRole.Role)
            {
                case Smod2.API.Role.SCP_173:
                    db.IncrementINT("class_scp173", steamid, table, 1);
                    break;

                case Smod2.API.Role.CLASSD:
                    db.IncrementINT("class_classd", steamid, table, 1);
                    break;

                case Smod2.API.Role.SCP_106:
                    db.IncrementINT("class_scp106", steamid, table, 1);
                    break;

                case Smod2.API.Role.NTF_SCIENTIST:
                    db.IncrementINT("class_ntf_scientist", steamid, table, 1);
                    break;

                case Smod2.API.Role.SCP_079:
                    db.IncrementINT("class_scp079", steamid, table, 1);
                    break;

                case Smod2.API.Role.CHAOS_INSURGENCY:
                    db.IncrementINT("class_ci", steamid, table, 1);
                    break;

                case Smod2.API.Role.SCP_096:
                    db.IncrementINT("class_scp096", steamid, table, 1);
                    break;

                case Smod2.API.Role.SCP_049_2:
                    db.IncrementINT("class_scp0492", steamid, table, 1);
                    break;

                case Smod2.API.Role.NTF_LIEUTENANT:
                    db.IncrementINT("class_ntfltn", steamid, table, 1);
                    break;

                case Smod2.API.Role.NTF_COMMANDER:
                    db.IncrementINT("class_ntfcmd", steamid, table, 1);
                    break;

                case Smod2.API.Role.NTF_CADET:
                    db.IncrementINT("class_ntf_cd", steamid, table, 1);
                    break;

                case Smod2.API.Role.TUTORIAL:
                    db.IncrementINT("class_tut", steamid, table, 1);
                    break;

                case Smod2.API.Role.FACILITY_GUARD:
                    db.IncrementINT("class_fguard", steamid, table, 1);
                    break;

                case Smod2.API.Role.SCP_939_53:
                    db.IncrementINT("class_scp939", steamid, table, 1);
                    break;

                case Smod2.API.Role.SCP_939_89:
                    db.IncrementINT("class_scp939", steamid, table, 1);
                    break;

                case Smod2.API.Role.SCIENTIST:
                    db.IncrementINT("class_scientist", steamid, table, 1);
                    break;
            }
        }

        public void OnWaitingForPlayers(WaitingForPlayersEvent ev)
        {
            DBConnection db = new DBConnection(main);
            db.AddTable(table);
        }

        public void OnRoundEnd(RoundEndEvent ev)
        {
            DBConnection db = new DBConnection(main);
            foreach(Player p in PluginManager.Manager.Server.GetPlayers())
            {
                db.IncrementINT("gameplayed", p.SteamId, table, 1);
            }
        }
    }
}